# Information / Информация

| Property     | Value                                              |
| ------------ | -------------------------------------------------- |
| ID           | `ext_034c9c79`                                     |
| Type         | Add-on                                             |
| License      | GPL-3.0                                            |
| Language     | Russian                                            |
| Requirements | XenForo 2.1                                        |
| Authors      | [Yu Dunaev](mailto:dun43v@gmail.com)               |

Система объявлений для [**XenForo**](https://xenforo.com).

## Install / Установка

1. [Загрузить](https://gitlab.com/marketplace-xenforo/xenforo-ext-notices/tags) архив с последней версией расширения.
2. Распаковать содержимое архива в `/src/addons/Marketplace/ext_034c9c79/`, сохраняя структуру директорий.
3. Зайти в **AdminCP**, далее *Add-ons*, и установить необходимое расширение.

## Update / Обновление

1. [Загрузить](https://gitlab.com/marketplace-xenforo/xenforo-ext-notices/tags) архив с новой версией расширения.
2. Распаковать содержимое архива в `/src/addons/Marketplace/ext_034c9c79/`, сохраняя структуру директорий, заменяя существующие файлы и папки.
3. Зайти в **AdminCP**, далее *Add-ons*, и обновить необходимое расширение.

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
